(use-modules (gnu))
(use-service-modules guix shepherd mcron)
(use-package-modules certs vim bash linux package-management)

(operating-system
  (host-name "openshift")
  (timezone "Europe/Paris")
  (locale "en_US.utf8")

  (bootloader (bootloader-configuration
               (bootloader grub-bootloader)
               (targets (list "empty"))))

  (file-systems (list (file-system
                        (device "empty")
                        (mount-point "/")
                        (type "empty"))))

  (packages (list guix-build-coordinator/agent-only nss-certs vim bash coreutils procps findutils grep))

  (services
    (append
      (modify-services %base-services
        (guix-service-type config =>
          (guix-configuration
           (substitute-urls
            '("https://bordeaux.guix.gnu.org"))
           (max-silent-time (* 24 3600))
           (timeout (* 48 3600))
           (chroot? #t)
           (authorized-keys
            (list
             (local-file "bordeaux.guix.gnu.org-export.pub")
             (local-file "data.guix.gnu.org.pub")
             (local-file "data.qa.guix.gnu.org.pub")))
           ;; In the openshift environment, Guile "sees" all cores of the
           ;; physical machine, and not only those available in a pod.
           ;; Pass the number of cores through an environment variable
           ;; set by openshift.
           (extra-options
             '((string-append "--cores=" (getenv "NB_CORES")))))))
      (list
        ;; For some unfathomable reason, the guix-service may or may
        ;; not store the three public keys above in /etc/guix/acl;
        ;; create this file manually.
        (simple-service 'acl etc-service-type
          (list `("guix/acl"
                 ,(local-file "acl"))))
        ;; guix-build-coordinator-agent-service-type requires a
        ;; networking service, which is silently provided in the
        ;; docker container. We just need to formally satisfy its
        ;; requirements.
        (simple-service 'dummy-networking shepherd-root-service-type
          (list
            (shepherd-service
              (documentation "dummy networking service")
              (start '(const #t))
              (provision '(networking)))))
        (service mcron-service-type
          (mcron-configuration
            (jobs (list #~(job '(next-hour '(7)) "guix gc -F 100G")))))
        (service guix-build-coordinator-agent-service-type
          (guix-build-coordinator-agent-configuration
           (coordinator
            "https://coordinator.bayfront.guix.gnu.org")
           (authentication
            (guix-build-coordinator-agent-dynamic-auth-with-file
             (agent-name "woodlands")
             (token-file
              "/etc/guix-build-coordinator/guix-build-coordinator-agent-token")))
             (extra-options
               '((string-append "--max-parallel-builds=" (getenv "GBC_PARALLEL_BUILDS"))
                 (string-append "--max-parallel-uploads=" (getenv "GBC_PARALLEL_UPLOADS"))))
           (systems '("x86_64-linux" "i686-linux"))
           (derivation-substitute-urls
            (list "https://data.guix.gnu.org"
                  "https://data.qa.guix.gnu.org"))
           (non-derivation-substitute-urls
            (list "https://bordeaux.guix.gnu.org"))))
))))
