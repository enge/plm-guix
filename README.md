# Guix on PLMshift

The goal of this experimental project is to create a compile farm for the
Guix project on the
[PLMshift](https://plmshift.pages.math.cnrs.fr/)
infrastructure. This
[gitlab project](https://gitlab.inria.fr/enge/plm-guix)
contains all things Guix; there is a
[corresponding PLMshift
project](https://plmshift.math.cnrs.fr/console/project/plm-guix/overview).


## Prepare the Guix system for docker

In the operating system definition, add a service
```
(service docker-service-type)
```
and add the user to the supplementary group `docker`.
Then install `docker-cli` as a package to the user profile.


## Create and use a Guix docker container

Configuration files can be found in the
[config
subdirectory](https://gitlab.inria.fr/enge/plm-guix/-/tree/master/config).
To create a docker image, modify in the Guix source tree the file
`gnu/system/linux-container.scm` so that the procedure
`containerized-operating-system` does not add `--disable-chroot` to the
parameters of the Guix daemon.
Then execute
```
./pre-inst-env guix system image -t docker plmshift.scm
```
The command results in an item in the store of the kind
`/gnu/store/yqk6w4b0f5dj6sw8wnkl3yhv0hynypn3-guix-docker-image.tar.gz`

The given image can be loaded into the local docker instance by executing
```
docker load -i /gnu/store/yqk6w4b0f5dj6sw8wnkl3yhv0hynypn3-guix-docker-image.tar.gz
```
It should then be visible by executing
```
docker images
```
with ```guix``` as repository and an abbreviated version of the hash as
image id.

```
ID=`docker create --privileged guix`
```
creates a docker container and stores its hash in ```ID```.
Privileged mode is needed so that the daemon can chroot.
```
docker container ls -a
```
shows all (running or not) containers.

```
docker inspect $ID
```
outputs the container definition in JSON format, in particular it shows
the entry point of the container, a list looking like
```
[ "/gnu/store/...-boot-program", "/gnu/store/...-system" ]
```
The associated command is empty.

This entrypoint is executed by
```
docker start $ID
```
The `...-boot-program` part starts the Guile interpreter to run the
script `boot` of the `...-system` part. This launches the shepherd
with a configuration file from the store, which in our case starts among
others the guix daemon inside the container.

To connect to the container, one can use
```
docker exec -it $ID /run/current-system/profile/bin/bash -l
```
to open a login root shell (the _login_ part is important since it sets
the path, otherwise not even common commands such as `ls` are available).

Inside the Guix image, the usual operations work, for instance
```
# guix package -i hello
# hello
```
downloads (or builds) the hello package, installs it into the user profile
and executes the command.

```
docker stop $ID
```
stops the running container, and
```
docker container prune
```
deletes all stopped containers and reclaims the corresponding memory.


## Start the build agent

The Guix daemon is already running in the container, since it has been
started by the shepherd. For the build agent, open a root shell in the
container and run the following command
```
export GUIX_BUILD_COORDINATOR_DYNAMIC_AUTH_TOKEN=...
export PATH=$PATH:/var/guix/profiles/system/profile/bin
guix-build-coordinator-agent --coordinator=https://coordinator.bayfront.guix.gnu.org --name=woodlands --max-parallel-builds=8 --max-parallel-uploads=1 --max-allocated-builds=48 --max-1min-load-average=8 --derivation-substitute-urls="https://data.guix.gnu.org https://data.qa.guix.gnu.org" --non-derivation-substitute-urls=https://bordeaux.guix.gnu.org --system=x86_64-linux --system=i686-linux
```
where ... has to be replaced by a valid secret token, created and printed
at the side of the Guix build coordinator with the command
```
guix-build-coordinator dynamic-auth create-token
```

## Towards creating a docker container by CI

Inside the ```config``` directory, the file ```channels.scm``` pins the
Guix version to a particular commit. From there, with any system containing
Guix, a docker container image (as above, as a ```.tar.gz``` in
```/gnu/store/...```) corresponding to the configuration can be
obtained with the following command:
```
guix time-machine -C channels.scm -- system image -t docker plmshift.scm
```

