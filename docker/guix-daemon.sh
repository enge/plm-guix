~/.config/guix/current/bin/guix-daemon --build-users-group=guixbuild &
GUIX_DAEMON=$!
exec "$@"
GUIX_RESULT=$?
kill -9 $GUIX_DAEMON
exit $GUIX_RESULT
